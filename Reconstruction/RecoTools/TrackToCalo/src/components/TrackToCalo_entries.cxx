#include "../ParticleCaloExtensionTool.h"
#include "../ParticleCaloCellAssociationTool.h"
#include "../MuonCaloEnergyTool.h"
#include "../CaloExtensionBuilderAlg.h"
#include "../PreselCaloExtensionBuilderAlg.h"

using namespace Trk;
using namespace Rec;
DECLARE_COMPONENT( ParticleCaloExtensionTool )
DECLARE_COMPONENT( ParticleCaloCellAssociationTool )
DECLARE_COMPONENT( MuonCaloEnergyTool )
DECLARE_COMPONENT( CaloExtensionBuilderAlg )
DECLARE_COMPONENT( PreselCaloExtensionBuilderAlg )
