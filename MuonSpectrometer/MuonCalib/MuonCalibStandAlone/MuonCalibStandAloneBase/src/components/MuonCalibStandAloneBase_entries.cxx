#include "MuonCalibStandAloneBase/CalibrationDummyIOTool.h"
#include "MuonCalibStandAloneBase/CalibrationTeeIOTool.h"
#include "MuonCalibStandAloneBase/RegionSelectionSvc.h"

using namespace MuonCalib;

DECLARE_COMPONENT(RegionSelectionSvc)
DECLARE_COMPONENT(CalibrationDummyIOTool)
DECLARE_COMPONENT(CalibrationTeeIOTool)
